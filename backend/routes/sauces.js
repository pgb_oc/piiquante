const express = require('express'); // express module import (app infrastructure)
const router = express.Router(); // router creation
const auth = require('../middlewares/auth'); // auth path, validation by token
const multer = require('../middlewares/multer-config'); // multer path, multer declared after auth
const saucesController = require('../controllers/sauce'); // Sauces path instructions


// Router CRUD

// auth middleware protect routes connec by auth token processus
// paths /api/sauce is reduced (routers)

// Create
router.post('/', auth, multer, saucesController.createSauces);
router.post('/:id/like', auth, saucesController.createVote);

// Read
router.get('/', auth, saucesController.getAllSauces);
router.get('/:id', auth, saucesController.getSauce);

// Update
router.put('/:id', auth, multer, saucesController.updateSauces);

// Delete
router.delete('/:id', auth, saucesController.deleteSauces);

module.exports = router;