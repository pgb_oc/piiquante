const express = require('express');
const router = express.Router();// router creation

const userController = require('../controllers/user'); // user path in controllers

router.post('/signup', userController.signup);
router.post('/login', userController.login);

module.exports = router; // router export