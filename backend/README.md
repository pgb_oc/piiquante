# HOT TAKES #

## Backend ##

install and add in package.json:

`npm install -g nodemon`

`npm install --save express`

`npm install --save body-parser`

`npm install --save mongoose`

Start and listen server:

`nodemon server`

Auth plugin:

`npm install --save mongoose-unique-validator`

Package bcrypt signup function:

`npm install --save bcrypt`

create and check token auth Payload

`npm install --save jsonwebtoken`

Implement multer - files download middleware

`npm install --save multer`

Security module:

Number requests limit by IP (brute force):

`npm install --save express-rate-limit`

Define http headers (9 middlewares sec functions)

`npm install --save helmet`

Sanitize request, security injections mongoDB

`npm install --save express-mongo-sanitize`

Load environment variables from .env:

`npm install --save dotenv`

## Debug ##

IF Error: spawn cmd ENOENT:

- Check if you have or add env var:
C:\Windows\System32

or try:

1. npm cache clean --force

2. Delete node_modules folder

3. delete package-lock.json

4.npm install 5. npm start

## Usage ##

Run and watch `nodemon start`. 

Use `Ctrl+C` in the terminal to stop the local server.


