const Sauce = require('../models/sauce'); // mongoose datas model import
const fs = require('fs'); // native file system manager import

/* ===================================================================================================== */
/* 01. CREATE                                                                                            */
/* ===================================================================================================== */

exports.createSauces = (req, res) => {
    const sauceObject = JSON.parse(req.body.sauce);
    delete sauceObject._id; // delete sauces Id
    const sauce = new Sauce({ // New instance of Sauce model
        ...sauceObject, // SPREAD Operator = all elements of req.body
        imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`,
    });
    sauce.save() // save Sauce in DB
        .then(() => res.status(201).json({ message: 'Sauce créée !' }))
        .catch(error => res.status(400).json({ error }));
    console.log(req.body);
};

exports.createVote = (req, res) => {
    let like = req.body.like
    let userId = req.body.userId
    let sauceId = req.params.id

    switch (like) {
        case 1: // user like sauce
            Sauce.updateOne({ _id: sauceId }, { $push: { usersLiked: userId }, $inc: { likes: +1 } }) // add userid and liked vote
                .then(() => res.status(200).json({ message: `J'aime` }))
                .catch((error) => res.status(400).json({ error }));

            break;

        case 0: // user vote reset for like/unlike 
            Sauce.findOne({ _id: sauceId })
                .then((sauce) => {
                    if (sauce.usersLiked.includes(userId)) { // Method .includes, if array have userId value, return true
                        Sauce.updateOne({ _id: sauceId }, { $pull: { usersLiked: userId }, $inc: { likes: -1 } }) // reset liked vote
                            .then(() => res.status(200).json({ message: `Vous n'avez pas encore voté !` }))
                            .catch((error) => res.status(400).json({ error }));
                    }
                    if (sauce.usersDisliked.includes(userId)) {
                        Sauce.updateOne({ _id: sauceId }, { $pull: { usersDisliked: userId }, $inc: { dislikes: -1 } }) // reset unliked vote
                            .then(() => res.status(200).json({ message: `Vous n'avez pas encore voté !` }))
                            .catch((error) => res.status(400).json({ error }));
                    }
                })
                .catch((error) => res.status(404).json({ error }));
            break;

        case -1: // user unlike sauce
            Sauce.updateOne({ _id: sauceId }, { $push: { usersDisliked: userId }, $inc: { dislikes: +1 } })
                .then(() => { res.status(200).json({ message: `Je n'aime pas` }) })
                .catch((error) => res.status(400).json({ error }));
            break;

        default:
            console.log(error);
    }
}

/* ===================================================================================================== */
/* 02. READ                                                                                              */
/* ===================================================================================================== */

exports.getSauce = (req, res) => {
    Sauce.findOne({ _id: req.params.id }) // method findOne, find the unique Sauce with _id
        .then(sauce => res.status(200).json(sauce))
        .catch(error => res.status(404).json({ error }));
    console.log(req.body);
};

exports.getAllSauces = (req, res, next) => {
    Sauce.find() // find method return [Sauces] from DB
        .then(sauces => res.status(200).json(sauces))
        .catch(error => res.status(400).json(error));
};

/* ===================================================================================================== */
/* 03. UPDATE                                                                                            */
/* ===================================================================================================== */

exports.updateSauces = (req, res) => {
    const sauceObject = req.file ? { // file exist or not
        ...JSON.parse(req.body.sauce),
        imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`// select base segment for server url, http protocol + hote + filename
    } : {
        ...req.body
    }
    // update selected sauce
    Sauce.updateOne({ _id: req.params.id }, { ...sauceObject, _id: req.params.id }) // method updateOne, Update Product with the same _id as param
        .then(() => res.status(200).json({ message: 'Sauce modifiée !' }))
        .catch(error => res.status(400).json({ error }));
};

/* ===================================================================================================== */
/* 04. DELETE                                                                                            */
/* ===================================================================================================== */

exports.deleteSauces = (req, res, next) => {
    Sauce.findOne({ _id: req.params.id }) // find sauce by id in db
        .then(sauce => {
            // sauce.imageUrl + static folder + index
            const filename = sauce.imageUrl.split('/images/')[1]; // select file name with index
            fs.unlink(`images/${filename}`, () => { // delete file with function unlink (fs module) + execute callback
                Sauce.deleteOne({ _id: req.params.id })// method deleteOne, Delete Product with _id as param
                    .then(() => res.status(200).json({ message: 'Sauce supprimée !' }))
                    .catch(error => res.status(400).json({ error }));
            })
        })
        .catch(error => res.status(500).json({ error }));
};