const bcrypt = require('bcrypt');
const User = require('../models/user');
const jwt = require('jsonwebtoken');

require('dotenv').config(); // dotenv (load env vars from .env file) module import (separe config)

// export function

exports.signup = (req, res, next) => {
    const passRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#?!@$%^&*-])[A-Za-z\d@$!%*?&]{8,}$/gm // password regex rules
    if (passRegex.exec(req.body.password) == null) {
        res.status(406).json({ message: 'Mot de passe invalide, votre mot de passe doit faire au minimum 8 caractères et doit comporter au minimum une majuscule, une minuscule, un nombre et un caractère spécial ex: #?!@$%^&*-' })
        return false
    }
    User.find() // find all users data's

        .then(users => {
            if (users.length === 0) {
                bcrypt.hash(req.body.password, 10) // Hash password x10 with bcrypt
                    .then(hash => {
                        const user = new User({ // user creation
                            email: req.body.email,
                            password: hash
                        })
                        bcrypt.hash(user.email, 10) // Hash user email
                            .then(hash => {
                                user.update(user.email = hash)
                                user.save() // save in db
                                    .then(user => {
                                        return res.status(201).json({ message: 'Utilisateur créé !' })
                                    })
                                    .catch(error => res.status(401).json({ message: 'Le processus de création de l\'utilisateur à échoué !' }));
                            })
                            .catch(error => res.status(500).json({ error }));
                    })
                    .catch(error => res.status(500).json({ error }));
            } else if (users.length >= 1) { // if database is not emty , check if user data's exist
                let error = {
                    userExistCounter: 0,
                    checkUserExist: 0
                };
                users.forEach(user => {
                    bcrypt.compare(req.body.email, user.email) // hash comparison
                        .then(valid => {
                            if (valid) {
                                error.userExistCounter++;
                                res.status(406).json({ message: 'Cette adresse email est déjà utilisée pour un compte existant. Merci d\'utiliser une adresse email différente.' })
                            } else if (!valid) {  // if comparison result not valid
                                error.checkUserExist++; // checkUserExist var increment
                                if (error.checkUserExist == users.length && error.userExistCounter == 0) { // if user not exist, new user creation
                                    bcrypt.hash(req.body.password, 10)
                                        .then(hash => {
                                            const user = new User({ //creating the new user
                                                email: req.body.email,
                                                password: hash
                                            })
                                            bcrypt.hash(user.email, 5) // hash email x5
                                                .then(hash => {
                                                    user.update(user.email = hash)
                                                    user.save()
                                                        .then(user => {
                                                            return res.status(201).json({ message: 'Utilisateur créé avec succès!' })
                                                        })
                                                        .catch(error => res.status(401).json({ message: 'Le processus de création de l\'utilisateur à échoué !' }));
                                                })
                                                .catch(error => res.status(500).json({ error }));
                                        })
                                        .catch(error => res.status(500).json({ error }));
                                }
                            }
                        })
                        .catch(error => res.status(500).json({ error }));
                })
            }
        })
        .catch(error => req.status(500).json({ error })); // mongo
}

exports.login = (req, res, next) => {
    User.find() // find all user's in db
        .then(users => {
            if (users.length == 0) {
                return res.status(401).json({ error: 'Utilisateur non trouvé !' })
            }
            let rejectCounter = 0;
            users.forEach(user => {
                bcrypt.compare(req.body.email, user.email) // compare emails entries with hash saved in db
                    .then(valid => {
                        if (!valid) { // if comparison result not valid
                            rejectCounter++; // rejet counter var increment
                            if (rejectCounter == users.length) {
                                return res.status(406).json({ error: 'Les identifiants ne correspondent a aucun compte !' })
                            }
                            return
                        }
                        bcrypt.compare(req.body.password, user.password) // compare user entry with hash saved in db
                            .then(valid => {
                                if (!valid) {
                                    return res.status(406).json({ error: 'Email ou password non valide !' })
                                }
                                else {
                                    res.status(200).json({ // user auth data's are ok
                                        userId: user._id, // id of user in db
                                        token: jwt.sign( // encode new token function sign, token auth security
                                            { userId: user._id }, // payload encoded data's in token
                                            process.env.JWT_PASS_TOKEN,
                                            //'RANDOM_uPc:{(2351))AzERpv11@896523@@,;14P78LsD&9*pP_SECRET', // secret token string 
                                            { expiresIn: '24 h' } // expiration delay
                                        )
                                    })
                                }
                            })
                            .catch(error => res.status(500).json({ error })); // internal error
                    })
                    .catch(error => res.status(500).json({ error }));
            })
        })
        .catch(error => req.status(500).json({ error })); // mongo error
}
