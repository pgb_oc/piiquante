// Prevent brute force attacks with express-rate-limit module

const reqRateLimit = require("express-rate-limit")
 
const limiter = reqRateLimit({
    max: 100, // limit number of requests by same user/IP
    windowMs: 60 * 60 * 1000, // 60 min delay
    message: "Too many request from this IP" // error message
});

// exporting
module.exports = limiter