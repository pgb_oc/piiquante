const jwt = require('jsonwebtoken');
require('dotenv').config(); // dotenv (load env vars from .env file) module import (separe config)

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        //const decodedToken = jwt.verify(token, 'RANDOM_uPc:{(2351))AzERpv11@896523@@,;14P78LsD&9*pP_SECRET');
        const decodedToken = jwt.verify(token, process.env.AUTH_TOKEN);
        const userId = decodedToken.userId;
        if (req.body.userId && req.body.userId !== userId) {
            throw 'Invalid user ID';
        } else {
            next();
        }
    } catch {
        res.status(403).json({
            error: new Error('403: unauthorized request.')
        });
    }
};