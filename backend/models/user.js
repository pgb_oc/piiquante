const mongoose = require('mongoose'); // import mongoose
const uniqueValidator = require('mongoose-unique-validator'); // module check if email is unique or not in db

const userSchema = mongoose.Schema({ // mongoose model for unique email
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true }
});

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model('user', userSchema);