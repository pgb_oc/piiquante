const express = require('express'); // express import
const app = express(); // express module storage
const mongoose = require('mongoose'); // mongoose ODM import

const bodyParser = require('body-parser'); // bodyParser import

const mongoSanitize = require('express-mongo-sanitize'); // sanitize data's, security mongoDB injection
const helmet = require('helmet');// helmet header sec import

const path = require('path');
require('dotenv').config(); // dotenv (load env vars) module import (separe config)

// call routes
const saucesRoutes = require('./routes/sauces');
const usersRoutes = require('./routes/user');

const limiter = require('./middlewares/rate-limits-sec'); // import req limiter

// MongoDB Atlas connexion

mongoose.connect( process.env.DB_CONNECT,
//mongoose.connect('mongodb+srv://pgb:LYRDLE27qpi8DoeP@cluster0.v6vsg.mongodb.net/Cluster0?retryWrites=true&w=majority',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log('Connexion à MongoDB réussie !'))
    .catch(() => console.log('Connexion à MongoDB échouée !'));

// CORS security
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*'); // All origins
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization'); // Allowed header
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS'); // Allowed verbs
    next();
});

// Define HTTP headers helmet security
app.use(helmet());
// Security: disable X-Powered-By header (auto in helmet module)
//app.disable('x-powered-by');

// Prevent MongoDB Operator Injection
app.use(mongoSanitize()); // sanitize the request (req.body, req.query, req.params)

// Global routes paths
app.use(bodyParser.json( { limit: "100kb"} )); // parse and limit payload size of requests
//app.use(bodyParser.urlencoded({ extended: true }));

app.use('/images', express.static(path.join(__dirname, 'images'))); // images storage location, 'images' is now static for express

// Global API routes
app.use('/api/sauces', limiter, saucesRoutes);
app.use('/api/auth', limiter, usersRoutes);

// Global 404 error
app.use(function(req, res, next) {
    res.status(404).send('Sorry cant find that!');
});

module.exports = app;
